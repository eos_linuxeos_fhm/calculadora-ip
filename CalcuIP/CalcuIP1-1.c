/* 
 * File:   CalcuIP.c
 * Author: eos
 *
 * Created on 13 de octubre de 2013, 16:59
 */

#include <stdio.h>
#include <stdlib.h>
#define ClaseA 1
#define ClaseB 2
#define ClaseC 3
#define error -3
#define correcto 15

/*
 * 
 */
union ip
{
    unsigned char DIRIP[3];
    unsigned int base;
   
}Nombresote,ayudita;
typedef struct dep
{
	char Nom[10];
	int host;//bits disponibles para subredes
	int bits;// bits disponibles para host
        int SubRed;//son los bits que se usaran en la subred
        int prefijoNue;// indica el nuevo prefijo a mostrar 
        union ip subredbit;//para manejar los bits de la subred de forma independiente
        union ip hostbit;//para manejar los bits de la host de forma independiente
        union ip basesota;//sobre el cual se realiza los calculos 
        union ip NombreRed;//las variables a mostrar 
        union ip Broadcast;//las variables a mostrar
}infodep;

int dep,prefijo,sum=1;;

void mostrador(infodep info);
int dquieredat(infodep *info,int ind);
int calculoBits(int host,int exp);
int exponente(int base, int exponencial);
int ordenador(infodep *lista);
int totalHost(int clase);
void CalSR(infodep *info,int ind);
void FormatoBinario(unsigned int x);


int main(void)
{
    int ind;
    //unsigned char Nombresote[3];
    infodep *lista;
    puts("Calculo de VLSM ");
    printf("deme la direccion IP Que desea -> ");
    scanf("%d.%d.%d.%d",&Nombresote.DIRIP[0],&Nombresote.DIRIP[1],&Nombresote.DIRIP[2],&Nombresote.DIRIP[3]);
    printf("%d.%d.%d.%d \n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3]);
    FormatoBinario(Nombresote.base);
    puts(" ");
    //Nombresote.base=Nombresote.base<<1;
    printf("Indique el Prefijo -> ");
    scanf("%d",&prefijo);
    if(valprefijo()==error)
    {
        return 0;
    }
    printf("%d.%d.%d.%d \n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3]);
    FormatoBinario(Nombresote.base);
    puts(" ");
    //printf(" bits %.8o.%.8o.%.8o.%.8u\n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3]);
    puts("indique cuantos departamentos quiere agregar ");
    scanf("%d",&dep);
    lista=(infodep *)malloc(sizeof(infodep)*dep);
    printf("--------------Adquiriendo datos de la red--------------------\n");
    for(ind=0;ind<dep;ind++)
    {
        if(adquieredat(lista,ind)==error)
        {
            ind=dep*5;
            printf("\nIntentelo nuevamente");
            return 0;
        }
    }
    ordenador(lista);
    /*puts("---------mostrando ordenado ---------");
    for(ind=0;ind<dep;ind++)
    {
        //adquieredat(lista,ind);
        mostrador(lista[ind]);  
        puts("----------------------------------------\n");
    }*/
    puts("---------Calculo de total de host---------");
    if(limitaciones(TotalHost(lista))==error)
    {
        printf("\nSe sobrepasa el limite de host para la clase que usted indico\n");
        return 0;
    }
    for(ind=0;ind<dep;ind++)
    {
        CalSR(lista,ind);
    }
    for(ind=0;ind<dep;ind++)
    {
        printf("\n\n****Realizando calculo****\n");
        CalculoMacabro(lista,ind);
    }
    puts("---------mostrando ordenado ---------");
    for(ind=0;ind<dep;ind++)
    {
        //adquieredat(lista,ind);
        mostrador(lista[ind]);  
        puts("----------------------------------------\n");
    }
    
    
    printf("Fin del calculo");
}
int CalculoMacabro(infodep *info,int posi)//adquirir la lista de los departamentos por cada departamento hacer los calculos 
{
    int ind=0;
    
    if(posi==0)//por si es el primer host de la lista 
    {
        info[posi].NombreRed.base=info[posi].hostbit.base=info[posi].subredbit.base=0;// se inicializa todo y ademas se guarda el valor corespondiente al la primera subred
        info[posi].hostbit.base=info[posi].hostbit.base |sum;//se sealiza la inicializacion del os bits para poder ser mostrado
        printf("indicacion de los bits de host\n");
        FormatoBinario(info[posi].hostbit.base);
        puts(" ");
        info[posi].basesota.base=Nombresote.base;//adquiere los datos de la ip original
        printf("ip basesota\n");
        printf("%d.%d.%d.%d*\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
        FormatoBinario(info[posi].basesota.base);
        puts(" ");
        printf("Corrimiento de los bits \n");
        for(ind=0;ind<=info[posi].bits-2;ind++)//para generar el prendido de todos los bits en en la seccion de host
        {
            //info[posi].hostbit.base=info[posi].hostbit.base |sum;
            printf("bits de hostbit corrimiento valor de indice %d\n",ind);
            printf("%d.%d.%d.%d*\n",info[posi].hostbit.DIRIP[0],info[posi].hostbit.DIRIP[1],info[posi].hostbit.DIRIP[2],info[posi].hostbit.DIRIP[3]);
            FormatoBinario(info[posi].hostbit.base);
            puts("");
            info[posi].hostbit.base=info[posi].hostbit.base<<1;//checar las posiciones de los bits
            
            printf("%d.%d.%d.%d*\n",info[posi].hostbit.DIRIP[0],info[posi].hostbit.DIRIP[1],info[posi].hostbit.DIRIP[2],info[posi].hostbit.DIRIP[3]);
            FormatoBinario(info[posi].hostbit.base);
            puts("");
            printf("bits de hostbit or\n");
            info[posi].hostbit.base=info[posi].hostbit.base |sum;
            printf("%d.%d.%d.%d*\n",info[posi].hostbit.DIRIP[0],info[posi].hostbit.DIRIP[1],info[posi].hostbit.DIRIP[2],info[posi].hostbit.DIRIP[3]);
            FormatoBinario(info[posi].hostbit.base);
            puts("");
        }
        //proceso de planchado de los bits 
        printf("\nProceso de planchado de bits\n");
        printf("bits de subredbit antes del corrimiento con relacion al numero de host << %d saltos \n",info[posi].bits);
        FormatoBinario(info[posi].subredbit.base);
        puts("");
        info[posi].subredbit.base=info[posi].subredbit.base<<info[posi].bits;
        printf("bits de subredbit despues del corrimiento con relacion al numero de host %d saltos \n",info[posi].bits);
        FormatoBinario(info[posi].subredbit.base);
        puts("");
        printf("\nla direccion ip de la base\n%d.%d.%d.%d**\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
        FormatoBinario(info[posi].basesota.base);
        puts(" ");
        printf("generacion del or para planchado con los bits de base y los bits de host\n");
        info[posi].basesota.base =info[posi].basesota.base|info[posi].hostbit.base;
        FormatoBinario(info[posi].basesota.base);
        puts(" ");
        printf("generacion del or para planchado con los bits de base y los bits de subredbit\n");
        info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
        FormatoBinario(info[posi].basesota.base);
        puts(" ");
        printf("bits de subredbit antes del corrimiento con relacion al numero de host >> %d saltos \n",info[posi].bits);
        info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
        FormatoBinario(info[posi].subredbit.base);
        puts(" ");
        printf("impresion de la base ya con cambios \n%d.%d.%d.%d**\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
        FormatoBinario(info[posi].basesota.base);
        puts(" ");

    }
    else
    {
        info[posi].hostbit.base=info[posi].subredbit.base=ayudita.base=0;//inicializamos los nuevos datos de los host en cero
        if(exponente(2,info[posi].bits)-1<exponente(2,info[posi-1].bits)-1)//checando si el valor de las redes que se piden es menor la de la posicion anterior se genere el calculo normal solo tomando encuenta el cambio de bits
        {
            info[posi].subredbit.base =info[posi-1].subredbit.base;//adquiere una copia de los bits de la red anterior 
            
            info[posi].basesota.base=info[posi-1].basesota.base;//adquiere los datos del host anterior para trabajar
            info[posi].basesota.base=info[posi].basesota.base<<(info[posi].SubRed-info[posi].SubRed);//el recorrimiento de los bits con relacion a la direfencia de bits entre la anterior 
            info[posi].hostbit.base=info[posi].hostbit.base |sum;
            
            printf("\n%d.%d.%d.%d***\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
            FormatoBinario(info[posi].basesota.base);
            puts(" ");
            for(ind=0;ind<=info[posi].bits;ind++)//para generar el prendido de todos los bits en en la seccion de host
            {
                info[posi].hostbit.base=info[posi].hostbit.base |sum;
                info[posi].hostbit.base=info[posi].hostbit.base<<1;//checar las posiciones de los bits 
                //info[posi].hostbit.base=info[posi].hostbit.base |sum;
            }
            //proceso de planchado de los bits 
            
            info[posi].subredbit.base=info[posi].subredbit.base<<info[posi].bits;
            info[posi].basesota.base =info[posi].basesota.base|info[posi].hostbit.base;
            info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
            info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
            printf("\n%d.%d.%d.%d****\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
            FormatoBinario(info[posi].basesota.base);
            puts(" ");
            
        }
        else // se hace un calculo normal al anterior pero con el desplazamiento de los datos 
        {
            info[posi].basesota.base=info[posi-1].basesota.base;//adquiere los datos de la subred anterior para trabajar
            info[posi].hostbit.base=info[posi].hostbit.base |sum;
            ///info[posi].basesota.base=Nombresote.base;//adquiere los datos de la ip original 
            printf("\n%d.%d.%d.%d*****\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
            for(ind=0;ind<=info[posi].bits;ind++)//para generar el prendido de todos los bits en en la seccion de host
            {
                info[posi].hostbit.base=info[posi].hostbit.base |sum;
                info[posi].hostbit.base=info[posi].hostbit.base<<1;//checar las posiciones de los bits 
                //info[posi].hostbit.base=info[posi].hostbit.base |sum;
            }
            //proceso de planchado de los bits 
            info[posi].subredbit.base=info[posi].subredbit.base<<info[posi].bits;
            info[posi].basesota.base =info[posi].basesota.base|info[posi].hostbit.base;
            info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
            info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
            printf("\n%d.%d.%d.%d******\n",info[posi].basesota.DIRIP[0],info[posi].basesota.DIRIP[1],info[posi].basesota.DIRIP[2],info[posi].basesota.DIRIP[3]);
        }
            
        
    }
}   
void CalSR(infodep *info,int ind)
{
    info[ind].SubRed=prefijo-info[ind].bits;//son los bits que se usaran en la subred calculandolo de los bits de host con reacion al prefijo
    info[ind].prefijoNue=info[ind].SubRed+prefijo;  //es la nueva mascara de la rede 
}

int valprefijo(void)
{
    if((prefijo>=8 && prefijo<=14) && TipoClase(Nombresote.DIRIP[0])==ClaseC )
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else if ((prefijo>=16 && prefijo<=30) && TipoClase(Nombresote.DIRIP[0])==ClaseB)
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else if((prefijo>=24 && prefijo<=48) && TipoClase(Nombresote.DIRIP[0])==ClaseA )
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else 
    {
        printf("Error del prefijo o mascara intente con otra\n");
        return error ;
    }
}
int TotalHost(infodep *lista)
{
    int ind,sum=0;
    for(ind=0;ind<dep;ind++)
    {
        sum=sum+lista[ind].host;
    }
    printf("El numero total de host a usar son %d\n el aproximado del total de host es %d\n",sum,exponente(2,calculoBits(sum,1))-1);
    //return calculoBits(sum,1);
    return exponente(2,calculoBits(sum,1))-1;
}
void mostrador(infodep info)
{
    printf("el nombre de la red es: %s \n",info.Nom);
    printf("el numero de host que pide es %d \n",info.host);
    printf("el numero de bits usados seran %d \n",info.bits);
    printf("el numero de host calculados sera %d \n",exponente(2,info.bits)-1);
    printf("el numero de bits para SR es de %d \n",info.SubRed);
    printf("%d.%d.%d.%d \\%d \n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3],prefijo);
    printf("el la subred es %d.%d.%d.%d \\%d \n",info.NombreRed.DIRIP[0],info.NombreRed.DIRIP[1],info.NombreRed.DIRIP[2],info.NombreRed.DIRIP[3],info.prefijoNue);
    printf("Direccion de Broadcast es %d.%d.%d.%d\n",info.Broadcast.DIRIP[0],info.Broadcast.DIRIP[1],info.Broadcast.DIRIP[2],info.Broadcast.DIRIP[3]);
}
int  adquieredat(infodep *info,int ind)
{
    printf("deme el nombre del departamento  -->");
    scanf("%s",info[ind].Nom);
    printf("deme cuantas computadoras quiere -->");
    scanf("%d",&info[ind].host);
    info[ind].host+=2;
    info[ind].bits=calculoBits(info[ind].host,1);
    if(limitaciones(info[ind].host)==error)
    {
        return error;
    }
}

int limitaciones(int host)
{
    if(TipoClase(Nombresote.DIRIP[0])==ClaseC && host > exponente(2,8))//numero de host para la claseC
    {
        
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o cambie la Clase de la red");
        return error;
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseB && host > exponente(2,16))//numero de host para la claseB 64K
    {
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o el cambie la Clase de la red");
        return error;
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseA && host > exponente(2,24))//numero de host para la claseA 16M
    {
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o el cambie la Clase de la red");
        return error;
    }
    else
    {
        printf("el numero de host es correcto\n");
        return 1;
    }
}

int calculoBits(int host,int exp)
{
    int corri=0;
    for(;;)
    {
        exp=exp<<1;
        if(exp>=host)
        {
            return corri+1;
        }
        else 
        {
            corri+=1;
        }
    }
}
int exponente(int base, int exponencial)
{
    if(exponencial==0)
        return 1;
    else
        return (exponente(base,exponencial-1)*base);
}
int TipoClase(int oct1)
{
    if(oct1>0 && oct1<=127)
        return ClaseA;
    else if(oct1>=128 && oct1<=191)
        return ClaseB;
    else if(oct1>=192 && oct1<=223)
        return ClaseC;
    else
        return error;
}
int ordenador(infodep *lista)
{
    infodep temp;
    int ind,ind2;
    for(ind2=0;ind2<dep-1;ind2++)
        for(ind=0;ind<dep-1;ind++)
        {
            if(lista[ind].host<lista[ind+1].host)
            {
                temp=lista[ind+1];
                lista[ind+1]=lista[ind];
                lista[ind]=temp;
            }
        }
}
char * format_binary(unsigned int x)
{
    static char b[33];
    b[32] = ' ';
    int z;
    for (z = 0; z < 32; z++) {
        b[31-z] = ((x>>z) & 0x1) ? '1' : '0';
    }

    return b;

}

void FormatoBinario(unsigned int x)
{
    static char b[33];
    b[32] = ' ';
    int z;
    for (z = 0; z < 32; z++) {
        b[31-z] = ((x>>z) & 0x1) ? '1' : '0';
    }
    for (z = 0; z < 32; z++)
    {
    	if(z%8==0)
    	{

    		printf(".");
    		printf("%c",b[z]);
    	}	
    	else
    	{
    	printf("%c",b[z]);
    	}
    }
}





//cuidar el prefijo con relacion al numero de departamentos que se pide


