/*
 * File:   CalcuIP.c
 * Author: eos
 *
 * Created on 13 de octubre de 2013, 16:59
 */
#include <stdio.h>
#include <stdlib.h>
#define ClaseA 1
#define ClaseB 2
#define ClaseC 3
#define error -3
#define correcto 15
union ip
{
    unsigned char DIRIP[3];
    unsigned int base;
}Nombresote;
typedef struct depa
{
	char Nom[10];
	int host;//bits disponibles para subredes
	int bits;// bits disponibles para host
    int SubRed;//son los bits que se usaran en la subred
    int prefijoNue;// indica el nuevo prefijo a mostrar
    union ip subredbit;//para manejar los bits de la subred de forma independiente
    union ip hostbit;//para manejar los bits de la host de forma independiente
    union ip basesota;//sobre el cual se realiza los calculos
    union ip NombreRed;//las variables a mostrar
    union ip Broadcast;//las variables a mostrar
}infodep;
int dep,prefijo,sum=1,sum2=0,sum1=0;
void mostrador(infodep info);
int dquieredat(infodep *info,int ind);
int calculoBits(int host,int exp);
int exponente(int base, int exponencial);
int ordenador(infodep *lista);
int totalHost(int clase);
void CalSR(infodep *info,int ind);
void FormatoBinario(unsigned int x);
unsigned int Traductor(unsigned int original );
int libertador(infodep *espartaco,int total);
void mostrador2(infodep info);
int main(void)
{
    int ind,libres=0,seg=0,opt=0;
    infodep *lista,*libre,*meganfox;
    puts("***********Calculo de VLSM***********");
    printf("Seleccione la opcion correcta\n1.- Modo Facil\n2.-Modo PRO\n-->>  ");
    scanf("%d",&opt);
    if(opt==2)
    {
        printf("deme la direccion IP Que desea -> ");
        scanf("%d.%d.%d.%d",&Nombresote.DIRIP[0],&Nombresote.DIRIP[1],&Nombresote.DIRIP[2],&Nombresote.DIRIP[3]);
        printf("%d.%d.%d.%d \n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3]);
        printf("Indique el Prefijo -> ");
        scanf("%d",&prefijo);
        prefijo=convertidor(prefijo);
        if(valprefijo()==error)
        {
            return 0;
        }
        puts("indique cuantos departamentos quiere agregar ");
        scanf("%d",&dep);
        lista=(infodep *)malloc(sizeof(infodep)*dep);
        libre=(infodep *)malloc(sizeof(infodep)*dep);
        for(ind=0;ind<dep;ind++)
        {
            libre[ind].Nom[0]=='2';
        }
        printf("--------------Adquiriendo datos de la red--------------------\n");
        for(ind=0;ind<dep;ind++)
        {
            if(adquieredat(lista,ind)==error)
            {
                ind=dep*5;
                printf("\nIntentelo nuevamente");
                return 0;
            }
        }
    }
    else if(opt==1)
    {
        Nombresote.DIRIP[0]=1;//se agrega una clase A por defecto
        Nombresote.DIRIP[1]=Nombresote.DIRIP[2]=Nombresote.DIRIP[3]=0;
        prefijo=8;
        prefijo=convertidor(prefijo);
        puts("indique cuantos departamentos quiere agregar ");
        scanf("%d",&dep);
        lista=(infodep *)malloc(sizeof(infodep)*dep);
        libre=(infodep *)malloc(sizeof(infodep)*dep);
        for(ind=0;ind<dep;ind++)
        {
            libre[ind].Nom[0]=='2';
        }
        printf("--------------Adquiriendo datos de la red--------------------\n");
        for(ind=0;ind<dep;ind++)
        {
            if(adquieredat(lista,ind)==error)
            {
                ind=dep*5;
                printf("\nIntentelo nuevamente");
                return 0;
            }
        }
        puts("--------------------------------------------------------------------");
        TotalHost(lista);
        puts("--------------------------------------------------------------------");
        if(sum2<exponente(2,8))//para recalcular el valor de la clase C
        {
            Nombresote.DIRIP[0]=192;
            Nombresote.DIRIP[1]=Nombresote.DIRIP[2]=Nombresote.DIRIP[3]=0;
            prefijo=24;
            prefijo=convertidor(prefijo);
        }
        else if(sum2<exponente(2,16))//para recalcular el valor de la clase B
        {
            Nombresote.DIRIP[0]=128;
            Nombresote.DIRIP[1]=Nombresote.DIRIP[2]=Nombresote.DIRIP[3]=0;
            prefijo=16;
            prefijo=convertidor(prefijo);
        }
        sum2=sum1=0;
    }
    else
    {
        puts("|\nErro se opcion que esta chamaco o que intentalo de nuevo ;)");
    }
    ordenador(lista);
    puts("\n\n---------Calculo de total de host---------");
    if(limitaciones(TotalHost(lista))==error)
    {
        printf("\nSe sobrepasa el limite de host para la clase que usted indico\n");
        return 0;
    }
    for(ind=0;ind<dep;ind++)
    {
        CalSR(lista,ind);
    }
    libertador(libre,sum2);

    for(ind=0;ind<dep;ind++)//genera los datos nesesarios para la red
    {
        CalSR(libre,ind);
    }
    for(ind=0;ind<dep;ind++)//indca cuantas son las que si se usan para el calculo de las nuevas redes
    {
        if(libre[ind].Nom[0]=='9')
        {
            libres=libres+1;
        }
    }
    meganfox=(infodep *)malloc(sizeof(infodep)*dep+libres);//se genera le nuevo arreglo a trabajar
    seg=libres;
    for(ind=0;ind<(dep+libres);ind++)//se juntan las lista de los departamentos con las libres
    {
        if(ind>=dep)
        {
            meganfox[ind]=libre[seg-1];
            seg=seg-1;
        }
        else
        {
            meganfox[ind]=lista[ind];
        }
    }for(ind=0;ind<dep+libres;ind++)//se genera el juntado de los departamentos con el calculo de las redes libres
    {
        CalculoMacabro(meganfox,ind);
    }
    puts("\n\n---------Resultado de los  Calculos ---------");
    puts("------------------------------------------------------------------------------------------------------\n");
    for(ind=0;ind<dep+libres;ind++)
    {
        printf("|  Nombre de Red    |  Direccion de Red     |   Direccion de Subred     |   Direccion de Boradcast     |\n\n");
        mostrador2(meganfox[ind]);
        puts("------------------------------------------------------------------------------------------------------\n");
    }
    puts("Fin del calculo");
}
int libertador(infodep *espartaco,int total)//TotalHost(lista) usar esta para mandar a llamar
{
    int resto,ind,a=3,cont=24,ind2;
    if(TipoClase(Nombresote.DIRIP[0])==ClaseC)
    {
        resto=exponente(2,8)-total;//numero de host para la claseC 255
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseB)
    {
        resto=exponente(2,16)-total;//numero de host para la claseB 64K
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseA )
    {
        resto=exponente(2,24)-total;//numero de host para la claseA 16M
    }
    for(ind=0;ind<dep;ind++)
    {
        //printf("\n****calculando bloques de restantes****\n");
        cont=24;
        while(a!=0)
        {
            if(resto>=0 && resto<=4)
            {
                return 0;
            }
            else if(exponente(2,cont)<=resto)
            {
                espartaco[ind].Nom[0]='9';
                espartaco[ind].host=exponente(2,cont);
                espartaco[ind].bits=calculoBits(espartaco[ind].host,1);
                resto=resto-exponente(2,cont);
                a=0;
            }
            cont=cont-1;
        }
        a=3;
    }
}
int CalculoMacabro(infodep *info,int posi)//adquirir la lista de los departamentos por cada departamento hacer los calculos
{
    int ind=0;
    int reco=0;
    if(posi==0)//por si es el primer host de la lista
    {
        info[posi].NombreRed.base=Nombresote.base;
        info[posi].hostbit.base=info[posi].subredbit.base=0;// se inicializa todo y ademas se guarda el valor corespondiente al la primera subred
        info[posi].hostbit.base=info[posi].hostbit.base |sum;//se realiza la inicializacion del los bits para poder ser mostrado
        info[posi].basesota.base=Traductor(Nombresote.base);//adquiere los datos de la ip original
        for(ind=0;ind<=info[posi].bits-2;ind++)//para generar el prendido de todos los bits en en la seccion de host
        {
            info[posi].hostbit.base=info[posi].hostbit.base<<1;//checar las posiciones de los bits
            info[posi].hostbit.base=info[posi].hostbit.base |sum;
        }
        info[posi].subredbit.base=info[posi].subredbit.base<<info[posi].bits;
        info[posi].basesota.base =info[posi].basesota.base|info[posi].hostbit.base;
        info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
        info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
        info[posi].Broadcast.base =Traductor(info[posi].basesota.base);
    }
    else//si no es la primera posicion
    {
        //cuando se cambia de tamaño de la rede o se realiza la siguente red
        info[posi].hostbit.base=info[posi].subredbit.base=0;//inicializamos los nuevos datos de los host en cero
        info[posi].subredbit.base=info[posi-1].basesota.base;
        reco=(24-info[posi].prefijoNue);
        if(reco<0)
            reco=reco*(-1);
        info[posi].subredbit.base=info[posi].subredbit.base<<reco;
        info[posi].subredbit.base=info[posi].subredbit.base>>reco;
        info[posi].subredbit.base=info[posi].subredbit.base+1;
        info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
        info[posi].basesota.base =info[posi].basesota.base|Traductor(Nombresote.base);
        info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
        info[posi].NombreRed.base=Traductor(info[posi].basesota.base);
        info[posi].hostbit.base=info[posi].hostbit.base |sum;
        for(ind=0;ind<=info[posi].bits-2;ind++)//para generar el prendido de todos los bits en en la seccion de host
        {
            info[posi].hostbit.base=info[posi].hostbit.base |sum;
            info[posi].hostbit.base=info[posi].hostbit.base<<1;//checar las posiciones de los bits
        }
        //proceso de planchado de los bits
        info[posi].subredbit.base=info[posi].subredbit.base<<info[posi].bits;
        info[posi].basesota.base =info[posi].basesota.base|info[posi].hostbit.base+1;
        info[posi].basesota.base =info[posi].basesota.base|info[posi].subredbit.base;
        info[posi].subredbit.base=info[posi].subredbit.base>>info[posi].bits;
        info[posi].Broadcast.base =Traductor(info[posi].basesota.base);
    }
}
void CalSR(infodep *info,int ind)
{
    info[ind].SubRed=prefijo-info[ind].bits;//son los bits que se usaran en la subred calculandolo de los bits de host con reacion al prefijo
    info[ind].prefijoNue=info[ind].SubRed+prefijo;  //es la nueva mascara de la red
}
int valprefijo(void)
{
    if((prefijo>=8 && prefijo<=30) && TipoClase(Nombresote.DIRIP[0])==ClaseC )
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else if ((prefijo>=16 && prefijo<=30) && TipoClase(Nombresote.DIRIP[0])==ClaseB)
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else if((prefijo>=24 && prefijo<=30) && TipoClase(Nombresote.DIRIP[0])==ClaseA )
    {
        printf("Prefijo valido\n");
        return correcto;
    }
    else
    {
        printf("Error del prefijo o mascara intente con otra\n");
        return error ;
    }
}
int TotalHost(infodep *lista)//genera la suma total de todos los host que se han pedido en todos los departamentos
{
    int ind;
    for(ind=0;ind<dep;ind++)
    {
        sum2=sum2+exponente(2,lista[ind].bits);
        sum1=sum1+lista[ind].host;
    }
    printf("El numero total de host a usar son %d\n el aproximado del total de host es %d\n",sum1,exponente(2,calculoBits(sum1,1))-1);
    return exponente(2,calculoBits(sum1,1))-1;
}
void mostrador(infodep info)
{
    if(info.Nom[0]=='9')
        printf("Redes libres\n");
    else
        printf("el nombre de la red es: %s \n",info.Nom);
    printf("el numero de host que pide es %d \n",info.host);
    printf("el numero de bits usados seran %d \n",info.bits);
    printf("el numero de host calculados sera %d \n",exponente(2,info.bits)-1);
    printf("el numero de bits para SR es de %d \n",info.SubRed);
    printf("%d.%d.%d.%d \\%d \n",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3],convertidor(prefijo));
    printf("la subred es %d.%d.%d.%d \\%d \n",info.NombreRed.DIRIP[0],info.NombreRed.DIRIP[1],info.NombreRed.DIRIP[2],info.NombreRed.DIRIP[3],convertidor(prefijo)+(info.prefijoNue-prefijo));
    printf("Direccion de Broadcast es %d.%d.%d.%d\n",info.Broadcast.DIRIP[0],info.Broadcast.DIRIP[1],info.Broadcast.DIRIP[2],info.Broadcast.DIRIP[3]);
}
void mostrador2(infodep info)
{
    if(info.Nom[0]=='9')
        printf("    Redes libres   %d |",exponente(2,info.bits));
    else
        printf("         %s       %d   |",info.Nom,exponente(2,info.bits));
    printf("    %d.%d.%d.%d \\%d   |",Nombresote.DIRIP[0],Nombresote.DIRIP[1],Nombresote.DIRIP[2],Nombresote.DIRIP[3],convertidor(prefijo));
    printf("     %d.%d.%d.%d \\%d  %d   |",info.NombreRed.DIRIP[0],info.NombreRed.DIRIP[1],info.NombreRed.DIRIP[2],info.NombreRed.DIRIP[3],convertidor(prefijo)+(info.prefijoNue-prefijo),info.host );
    printf("       %d.%d.%d.%d         |\n",info.Broadcast.DIRIP[0],info.Broadcast.DIRIP[1],info.Broadcast.DIRIP[2],info.Broadcast.DIRIP[3]);
}
int  adquieredat(infodep *info,int ind)
{
    printf("deme el nombre del departamento  -->");
    scanf("%s",info[ind].Nom);
    printf("deme cuantas computadoras quiere -->");
    scanf("%d",&info[ind].host);
    info[ind].host+=2;
    info[ind].bits=calculoBits(info[ind].host,1);
    if(limitaciones(info[ind].host)==error)
        return error;
    if(info[ind].host==exponente(2,info[ind].bits))
        info[ind].bits+=1;
}
int limitaciones(int host)//controla el numero total de host por cada tipo de clase
{
    if(TipoClase(Nombresote.DIRIP[0])==ClaseC && host > exponente(2,8))//numero de host para la claseC 255
    {
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o cambie la Clase de la red");
        return error;
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseB && host > exponente(2,16))//numero de host para la claseB 64K
    {
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o el cambie la Clase de la red");
        return error;
    }
    else if(TipoClase(Nombresote.DIRIP[0])==ClaseA && host > exponente(2,24))//numero de host para la claseA 16M
    {
        printf(" error el el numero de host sobrepasa lo permitodo \n\n introdusca menos host o el cambie la Clase de la red");
        return error;
    }
    else
    {
        printf("el numero de host es correcto\n");
        return 1;
    }
}
int calculoBits(int host,int exp)
{
    int corri=0;
    for(;;)
    {
        exp=exp<<1;
        if(exp>=host)
            return corri+1;
        else
            corri+=1;
    }
}
int exponente(int base, int exponencial)
{
    if(exponencial==0)
        return 1;
    else
        return (exponente(base,exponencial-1)*base);
}
int TipoClase(int oct1)
{
    if(oct1>0 && oct1<=127)
        return ClaseA;
    else if(oct1>=128 && oct1<=191)
        return ClaseB;
    else if(oct1>=192 && oct1<=223)
        return ClaseC;
    else
        return error;
}
int ordenador(infodep *lista)
{
    infodep temp;
    int ind,ind2;
    for(ind2=0;ind2<dep-1;ind2++)
        for(ind=0;ind<dep-1;ind++)
        {
            if(lista[ind].host<lista[ind+1].host)
            {
                temp=lista[ind+1];
                lista[ind+1]=lista[ind];
                lista[ind]=temp;
            }
        }
}
void FormatoBinario(unsigned int x)
{
    static char b[33];
    b[32] = ' ';
    int z;
    for (z = 0; z < 32; z++)
        b[31-z] = ((x>>z) & 0x1) ? '1' : '0';
    for (z = 0; z < 32; z++)
    {
    	if(z%8==0)
    	{
            printf(".");
            printf("%c",b[z]);
    	}
    	else
    	{
    	printf("%c",b[z]);
    	}
    }
}
unsigned int Traductor(unsigned int original )// si la version es 1
{
    unsigned int respaldo=0,temp=0;
    temp=temp|original;
    temp=temp<<24;
    temp=temp>>24;
    respaldo=respaldo|temp;
    respaldo=respaldo<<8;
    //---------------------------------
    original=original>>8;
    temp=0;
    temp=temp|original;
    temp=temp<<24;
    temp=temp>>24;
    respaldo=respaldo|temp;
    respaldo=respaldo<<8;
    //----------------------------------
    original=original>>8;
    temp=0;
    temp=temp|original;
    temp=temp<<24;
    temp=temp>>24;
    respaldo=respaldo|temp;
    respaldo=respaldo<<8;
    //----------------------------------
    original=original>>8;
    temp=0;
    temp=temp|original;
    temp=temp<<24;
    temp=temp>>24;
    respaldo=respaldo|temp;
    //----------------------------------
    return respaldo;
}
int convertidor(int modificado)
{
    return 32-modificado;
}

